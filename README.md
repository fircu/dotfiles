# Dotfiles

Output repository status if in a git repository.

## Install

1. Run `./install.sh` and follow instructions

```bash
# Enable tab completion
if [ -f ~/dotfiles/git/git-completion.bash ]; then
	source ~/dotfiles/git/git-completion.bash
else
	echo "Cannot source ~/dotfiles/git/git-completion.bash"
fi

# colors!
green="\[\033[0;32m\]"
light_green="\[\033[1;32m\]"
blue="\[\033[0;34m\]"
light_blue="\[\033[1;34m\]"
purple="\[\033[0;35m\]"
white="\[\033[1;37m\]"
yellow="\[\033[1;33m\]"
brown_orange="\[\033[0;33m\]"
reset="\[\033[0m\]"

# Change command prompt
if [ -f ~/dotfiles/git/git-prompt.sh ]; then
	source ~/dotfiles/git/git-prompt.sh
else
	echo "Cannot source ~/dotfiles/git/git-prompt.sh"
fi

# Show unstaged (*) and (+) staged next to the branch name
export GIT_PS1_SHOWDIRTYSTATE=1

# Show stashed status ($) next to the branch name
export GIT_PS1_SHOWSTASHSTATE=1

# Show if there any untracked files (%) next to the branch name
export GIT_PS1_SHOWUNTRACKEDFILES=1

# '\u' adds the name of the current user to the prompt
# '\h' adds the name of the current machine
# '\$(__git_ps1)' adds git-related stuff
# '\W' adds the name of the current directory
export PS1="$yellow\u@\h$light_green\$(__git_ps1)$white \W $ $reset"

```
