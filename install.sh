#!/bin/bash
###########################################################
# Written by Adrian Firculescu
#
# Automate config for git prompt and git completion in bash
###########################################################

# Check if parent ~/dotfiles/git directory exists
if [ ! -d ~/dotfiles/git ]; then
	echo "##########################"
	echo "Creating ~/dotfiles/git..."
	echo "##########################"
	mkdir -p ~/dotfiles/git
fi

# Check for git-completion.bash; download it from git sources
if [ ! -f ~/dotfiles/git/git-completion.bash ]; then
	echo "##################################"
	echo "Downloading git-completion.bash..."
	echo "##################################"
	wget -P ~/dotfiles/git/ \
		https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
fi

# Check for git-prompt.sh; download it from git sources
if [ ! -f ~/dotfiles/git/git-prompt.sh ]; then
	echo "############################"
	echo "Downloading git-prompt.sh..."
	echo "############################"
	wget -P ~/dotfiles/git/ \
		https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
fi

# git-shell.sh should be included with this script
if [ ! -f ~/dotfiles/git/git-shell.sh ]; then
	echo "#######################"
	echo "Copying git-shell.sh..."
	echo "#######################"

	if [ ! -f git-shell.sh ]; then
		echo "Cannot find git-shell.sh"
		echo "Please make sure you have it under the same directory \
			as this script"
		exit 1
	fi

	cp ./git-shell.sh ~/dotfiles/git/
fi
echo "#################################################"
echo "Appending git-aware completion to your .bashrc..."
echo "#################################################"
echo ""
read -p "WARNING! This will overwrite your prompt. Do you want to continue? [y/N] " yn

if [[ "$yn" != "y" && "$yn" != "yes" ]]; then
	echo "Aborted... Type 'y' or 'yes' confirm."
	exit 1
fi

# Append to user home env

cat ~/.bashrc | grep git-shell >/dev/null 2>&1

if [ $? -gt 0 ]; then
	echo "" >> ~/.bashrc
	echo "# Enable git-aware prompt and git-completion" >> ~/.bashrc
	echo "if [ -f ~/dotfiles/git/git-shell.sh ]; then" >> ~/.bashrc
	echo "	source ~/dotfiles/git/git-shell.sh" >> ~/.bashrc
	echo "fi" >> ~/.bashrc

	echo "################################################"
	echo "Successfully appended git-shell.sh to your .bashrc"
	echo "################################################"
else
	echo "#####################################################"
	echo "It looks like you're already sourcing ~/dotfiles/git/git-shell.sh"
	echo "#####################################################"
fi
